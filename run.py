# run.py
from flask import Flask, jsonify, render_template, request, redirect, url_for
import datetime as dt
import sys, pathlib
directory = pathlib.Path(__file__).parent
sys.path.insert(1, str(directory))

from data.logic import fetchStreams

meta = {
    'title': 'Live Tracker',
    'logo': 'https://media.npr.org/chrome_svg/npr-logo-color.svg',
    'primaryImg': 'https://media.npr.org/chrome_svg/npr-logo-color.svg',
    'version': '0.0.0',
    'pyVersion': str(sys.version_info.major)+'.'+str(sys.version_info.minor)+' on '+sys.prefix,
    'year': dt.datetime.today().strftime('%Y')
       }

app = Flask(__name__)
application = app #wsgi doesn't like abbreviations

@app.route('/')
def index():
    return redirect(url_for('.top', num='10'))

@app.route('/<num>')
def top(num):
    #num = int(request.args.get('num'))
    if num == '10':
        call_letters = 'WNYC,WNYC-AM,KCRW,KPCC,WBEZ,KQED,KALW,KERA,KUHF,WAMU'
    elif num == '20':
        call_letters = 'WABE,WGPB,WHYY,WBUR,WGBH,WLRN,KUOW,KNKX,WDET,KJZZ'
    elif num == '30':
        call_letters = 'KNOW,KPBS,KCFR,KUNC,WUSF,WYPR,KOPB,KWMU,WFAE,KVCR'
    elif num == '40':
        call_letters = 'KSTX,KXJZ,WESA,KUER,KNPR,WMFE,WVXU,KUT,WCPN,KCUR'
    elif num == '50':
        call_letters = 'WOSU,WUNC,WFYI,WAMC,WUWM,WERN'
    return render_template('index.html',
                           active_id = num,
                           stations = fetchStreams(call_letters),
                           meta = meta)

@app.errorhandler(Exception)
def exception_handler(error):
    return repr(error)
