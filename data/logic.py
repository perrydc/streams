import npr

def fetchStreams(call_letters):
    streamList = []
    call_list = call_letters.split(",")
    for call in call_list:
        station = npr.Stations(call)
        market = station.response['items'][0]['attributes']['brand']['marketCity']
        stream = station.unwrap(station.stream)
        streamList.append({'call':call,'stream':stream, 'city':market})
    return streamList